<?php

namespace Core\Domain\Entity;

class Ad extends AbstractEntity
{
    const STATUS_WAIT_FOR_APPROVE = 0;
    const STATUS_APPROVED = 1;
    const STATUS_OLD_VERSION = 2;

    protected $owner;
    protected $timeOfChange;
    protected $authorOfChange;
    protected $status;
    protected $parameters;

    public function getOwner()
    {
        return $this->owner;
    }

    public function setOwner($owner) : Ad
    {
        $this->owner = $owner;
        return $this;
    }

    public function getTimeOfChange() : \DateTime
    {
        return $this->timeOfChange;
    }

    public function setTimeOfChange(\DateTime $timeOfChange) : Ad
    {
        $this->timeOfChange = $timeOfChange;
        return $this;
    }

    public function getAuthorOfChange()
    {
        return $this->authorOfChange;
    }

    public function setAuthorOfChange($authorOfChange)
    {
        $this->authorOfChange = $authorOfChange;
        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    public function getParameters()
    {
        return $this->parameters;
    }

    public function setParameters($parameters)
    {
        $this->parameters = $parameters;
        return $this;
    }

}
