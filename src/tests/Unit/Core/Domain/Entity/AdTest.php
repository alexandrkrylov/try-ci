<?php

namespace Tests\Unit\Core\Domain\Entity;

use PHPUnit\Framework\TestCase;
use Core\Domain\Entity\Ad;

class AdTest extends TestCase
{
    public function testSetTimeOfChange()
    {
        $ad = new Ad();
        $this->assertInstanceOf(
            get_class($ad), $ad->setTimeOfChange(new \DateTime())
        );
    }
}
