# BANKET.kz with docker and clean architecture
>![banket logo](src/public/img/logo.png "banket logo")
# System requirements

For local application starting (for development) make sure that you have locally installed next applications:
* docker >= 18.0 (install: curl -fsSL get.docker.com | sudo sh)
* docker-compose >= 1.22 (installing manual)
* make >= 4.1 (install: apt-get install make)